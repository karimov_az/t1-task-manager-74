package ru.t1.karimov.tm;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
