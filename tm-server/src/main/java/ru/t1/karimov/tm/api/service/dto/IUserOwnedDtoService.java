package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedDtoModel> extends IDtoService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Long getSize(@Nullable String userId);

    void removeAll(@Nullable String userId);

    void removeOne(@Nullable String userId, M model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void update(@Nullable String userId, M model);

}
