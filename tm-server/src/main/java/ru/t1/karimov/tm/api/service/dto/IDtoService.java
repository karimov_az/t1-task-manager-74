package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.List;

public interface IDtoService<M extends AbstractDtoModel> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    boolean existsById(@Nullable String id);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    Long getSize();

    void removeAll();

    void removeOne(@Nullable M model);

    void removeOneById(@Nullable String id);

    void removeOneByIndex(@Nullable Integer index);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void update(@Nullable M model);

}
