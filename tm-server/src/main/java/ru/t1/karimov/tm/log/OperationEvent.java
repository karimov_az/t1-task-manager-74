package ru.t1.karimov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @NotNull
    private String table;

    @NotNull
    private final Long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
