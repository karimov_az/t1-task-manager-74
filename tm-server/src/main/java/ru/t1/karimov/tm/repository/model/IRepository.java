package ru.t1.karimov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.karimov.tm.model.AbstractModel;

@NoRepositoryBean
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}

