package ru.t1.karimov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.user.UserRemoveRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "User remove.";

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOCK");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        userEndpoint.removeUser(request);
    }

}
