package ru.t1.karimov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.request.project.ProjectGetByIndexRequest;
import ru.t1.karimov.tm.dto.response.project.ProjectGetByIndexResponse;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Show project by index.";

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[PROJECT LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final ProjectGetByIndexResponse response = projectEndpoint.getProjectByIndex(request);
        @Nullable final ProjectDto project = response.getProject();
        showProject(project);
    }

}
