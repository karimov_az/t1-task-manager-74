package ru.t1.karimov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.project.ProjectClearRequest;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProjects(request);
    }

}
